﻿using System;
using HelloWorldOpen.Bot.Messages;
using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Abstract
{
    public abstract class SendMessage
    {
        [JsonProperty("msgType")]
        public string MessageType { get; set; }

        public int GameTick { get; set; }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MessageWrapper(this.MessageType, this.GetData(), this.GameTick));
        }

        protected virtual Object GetData()
        {
            return this;
        }
    }
}