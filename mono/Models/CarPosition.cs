﻿using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Models
{
    public class CarPosition
    {
        [JsonProperty("id")]
        public Car Car { get; set; }

        [JsonProperty("angle")]
        public float Angle { get; set; }

        [JsonProperty("piecePosition")]
        public PiecePosition PiecePosition { get; set; }
    }
}
