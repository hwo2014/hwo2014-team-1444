﻿using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Models
{
    public class StartingPoint
    {
        [JsonProperty("position")]
        public Position Position { get; set; }

        [JsonProperty("angle")]
        public float Angle { get; set; }
    }
}
