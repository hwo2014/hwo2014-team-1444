﻿using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Models
{
    public class Race
    {
        [JsonProperty("track")]
        public Track Track { get; set; }

        [JsonProperty("cars")]
        public Contestant[] Contestants { get; set; }

        [JsonProperty("raceSession")]
        public RaceSession RaceSession { get; set; }
    }
}
