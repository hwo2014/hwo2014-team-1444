﻿using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Models
{
    public class Position
    {
        [JsonProperty("x")]
        public float X { get; set; }

        [JsonProperty("y")]
        public float Y { get; set; }
    }
}
