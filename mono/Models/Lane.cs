﻿using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Models
{
    public class Lane
    {
        [JsonProperty("startLaneIndex")]
        public int StartLaneIndex { get; set; }

        [JsonProperty("endLaneIndex")]
        public int EndLaneIndex { get; set; }
    }
}
