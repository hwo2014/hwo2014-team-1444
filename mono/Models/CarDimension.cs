﻿using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Models
{
    public class CarDimension
    {
        [JsonProperty("length")]
        public float Length { get; set; }

        [JsonProperty("width")]
        public float Width { get; set; }

        [JsonProperty("guideFlagPosition")]
        public float GuideFlagPosition { get; set; }
    }
}
