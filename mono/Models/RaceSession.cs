﻿using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Models
{
    public class RaceSession
    {
        [JsonProperty("laps")]
        public int Laps { get; set; }

        [JsonProperty("maxLapTimeMs")]
        public int MaxLapTimeMs { get; set; }

        [JsonProperty("quickRace")]
        public bool QuickRace { get; set; }
    }
}
