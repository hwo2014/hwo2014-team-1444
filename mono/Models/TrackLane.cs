﻿using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Models
{
    public class TrackLane
    {
        [JsonProperty("distanceFromCenter")]
        public int DistancefromCenter { get; set; }

        [JsonProperty("index")]
        public int Index { get; set; }
    }
}
