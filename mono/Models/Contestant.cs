﻿using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Models
{
    public class Contestant
    {
        [JsonProperty("id")]
        public Car Car { get; set; }

        [JsonProperty("dimensions")]
        public CarDimension CarDimension { get; set; }
    }
}
