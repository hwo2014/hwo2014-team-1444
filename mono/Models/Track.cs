﻿using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Models
{
    public class Track
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("pieces")]
        public Piece[] Pieces { get; set; }

        [JsonProperty("lanes")]
        public TrackLane[] Lanes { get; set; }

        [JsonProperty("startingPoint")]
        public StartingPoint StartingPoint { get; set; }
    }
}
