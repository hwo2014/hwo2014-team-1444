﻿using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Models
{
    public class Car
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("color")]
        public string Color { get; set; }
    }
}
