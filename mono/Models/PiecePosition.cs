﻿using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Models
{
    public class PiecePosition
    {
        [JsonProperty("pieceIndex")]
        public int Index { get; set; }

        [JsonProperty("inPieceDistance")]
        public float InPieceDistance { get; set; }

        [JsonProperty("lane")]
        public Lane Lane { get; set; }

        [JsonProperty("lap")]
        public int Lap { get; set; }

    }
}
