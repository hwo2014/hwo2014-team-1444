﻿using System.Collections.Generic;

namespace HelloWorldOpen.Bot.Models
{
    public class CarTelemetry
    {
        public CarTelemetry()
        {
            this.CarPositions = new Stack<CarPosition>();
        }

        public Stack<CarPosition> CarPositions { get; set; }
    }
}
