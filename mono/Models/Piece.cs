﻿using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Models
{
    public class Piece
    {
        [JsonProperty("length")]
        public float Length { get; set; }

        [JsonProperty("angle")]
        public float Angle { get; set; }

        [JsonProperty("switch")]
        public bool Switch { get; set; }

        [JsonProperty("bridge")]
        public bool Bridge { get; set; }

        [JsonProperty("radius")]
        public int Radius { get; set; }
    }
}
