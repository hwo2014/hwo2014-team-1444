﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using HelloWorldOpen.Bot.Abstract;
using HelloWorldOpen.Bot.Helpers;
using HelloWorldOpen.Bot.Messages;
using HelloWorldOpen.Bot.Models;
using HelloWorldOpen.Bot.Networking;
using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Managers
{
    public sealed class GameManager : IDisposable
    {
        private ITcpClient _tcpClient;
        private INetworkStream _networkStream;
        private IMessageReciever _messageReciever;
        private IMessageTransmitter _messageTransmitter;

        public GameManager(string botName, string botKey)
        {
            this.BotName = botName;
            this.BotKey = botKey;
            TelemetryManager = new TelemetryManager(this);
        }

        #region States

        public bool GameStart { get; set; }

        #endregion

        public Bot AI { get; set; }

        public Race Race { get; set; }

        public Track Track
        {
            get
            {
                return Race.Track;
            }
        }

        public Piece[] TrackPieces
        {
            get
            {
                return Track.Pieces;
            }
        }

        public int TrackLength
        {
            get
            {
                return TrackPieces.Length;
            }
        }
        
        public TelemetryManager TelemetryManager { get; set; }

        public string BotName { get; set; }

        public string BotKey { get; set; }

        public void Connect(string host, int port)
        {
            _tcpClient = new GameTcpClient(host, port);
            _networkStream = _tcpClient.GetStream();
            _messageReciever = _networkStream.GetMessageReciever();
            _messageTransmitter = _networkStream.GetMessageTransmitter();
            AI = new Bot(this);
        }

        public void Join(Join message)
        {
            this.SendMessage(message);
            this.StartGameLoop();
        }

        private void StartGameLoop()
        {
            string line;
            while ((line = _messageReciever.RecieveMessage()) != null)
            {
                var message = JsonConvert.DeserializeObject<MessageWrapper>(line);
                switch (message.MessageType)
                {
                    case "join":
                        //this.HandleJoin(message.Deserialize<Join>());
                        this.HandleJoin(null);
                        break;
                    case "gameInit":
                        //this.HandleGameInit(message.Deserialize<GameInit>());
                        this.HandleGameInit(message.Deserialize<GameInit>());
                        break;
                    case "gameStart":
                        //this.HandleRaceStart(message.Deserialize<GameStart>());
                        this.HandleRaceStart(null);
                        break;
                    case "carPositions":
                        this.HandleCarPositions(message);
                        break;
                        //TODO: crash
                    case "gameEnd":
                        //this.HandleRaceEnded(message.Deserialize<GameEnd>());
                        this.HandleRaceEnded(null);
                        break;
                    default:
                        if (!string.IsNullOrEmpty(message.MessageType))
                        {
                            Console.WriteLine(message.MessageType);
                        }
                        this.SendMessage(new Ping());
                        break;
                }
            }
        }

        public void SendMessage(SendMessage message)
        {
            this._messageTransmitter.Transmit(message.ToJson());
        }

        private void HandleJoin(Join message)
        {
            Console.WriteLine("Joined");
            this.SendMessage(new Ping());
        }

        private void HandleGameInit(GameInit gameInit)
        {
            Console.WriteLine("Race init");
            this.Race = gameInit.Race;
            //TODO calculate optimized speed on wild guess
            this.SendMessage(new Ping());
        }

        private void HandleRaceEnded(GameEnd message)
        {
            Console.WriteLine("Race ended");
            this.SendMessage(new Ping());
        }

        private void HandleRaceStart(GameStart message)
        {
            GameStart = true;
            Console.WriteLine("Race starts");
            this.SendMessage(new Ping());
        }

        private void HandleCarPositions(MessageWrapper message)
        {
            HandleCarPositions(message.GameId, message.GameTick, message.Deserialize<CarPosition[]>());
        }

        private void HandleCarPositions(string gameId, int gameTick, CarPosition[] carPositions)
        {
            CarPosition myCar = carPositions[0];
            Piece currentPiece = TrackPieces[myCar.PiecePosition.Index];
            TelemetryManager.CollectCarPositionData(BotName, gameTick, myCar, currentPiece);
            float currentSpeed = TelemetryManager.GetCurrentSpeed();
            Piece[] nextTrackPieces = this.GetNextTrackPieces(myCar.PiecePosition.Index);
            AI.Throttle(gameTick, currentSpeed, nextTrackPieces);
        }

        private Piece[] GetNextTrackPieces(int startIndex)
        {
            return new Piece[]
                   {
                       TrackPieces[TrackPieceIndexHelper.GetTrackIndex(startIndex, TrackLength)],
                       TrackPieces[TrackPieceIndexHelper.GetTrackIndex(startIndex+1, TrackLength)],
                       TrackPieces[TrackPieceIndexHelper.GetTrackIndex(startIndex+2, TrackLength)],
                       TrackPieces[TrackPieceIndexHelper.GetTrackIndex(startIndex+3, TrackLength)],
                       TrackPieces[TrackPieceIndexHelper.GetTrackIndex(startIndex+4, TrackLength)],
                   };
        }

        #region IDisposable

        public void Dispose()
        {
            this.Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_tcpClient != null)
                {
                    _tcpClient.Dispose();
                    _tcpClient = null;
                }
            }
        }

        #endregion
    }
}
