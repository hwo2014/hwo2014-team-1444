﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection.Emit;
using HelloWorldOpen.Bot.Helpers;
using HelloWorldOpen.Bot.Messages;
using HelloWorldOpen.Bot.Models;

namespace HelloWorldOpen.Bot.Managers
{
    public class TelemetryManager : IDisposable
    {
        private FileStream _fileStream;
        private StreamWriter _streamWriter;

        public TelemetryManager(GameManager gameManager)
        {
            this.GameManager = gameManager;
            CarTelemetries = new Dictionary<string, CarTelemetry>();
            CarTelemetries.Add(GameManager.BotName, new CarTelemetry());

            _fileStream = File.OpenWrite(string.Format("telemetry-{0}.csv", DateTime.Now.Ticks));
            _streamWriter = new StreamWriter(_fileStream);
            //_streamWriter.WriteLine("GameId;GameTick;CarAngle;CurrentPieceLength;CurrentPieceAngle;CurrentPieceSwitch;PiecePositionIndex;PiecePositionInPieceDistance;Throttle");
            _streamWriter.WriteLine("ContestantName;GameTick;Lap;CarAngle;PieceIndex;PieceLength;PieceAngle;PieceRadius;Switch;InPieceDistance;");
        }

        protected Dictionary<string, CarTelemetry> CarTelemetries { get; set; }

        protected GameManager GameManager { get; set; }

        public CarTelemetry MyCarTelemetry
        {
            get
            {
                return CarTelemetries[GameManager.BotName];
            }
        }

        public void AddContestant(string contestantName)
        {
            this.CarTelemetries.Add(contestantName, new CarTelemetry());
        }

        public float GetCurrentSpeed()
        {
            if(MyCarTelemetry.CarPositions.Count < 2)
            {
                return 0.0f;
            }
            // Data
            CarPosition currentTick = MyCarTelemetry.CarPositions.Pop();
            CarPosition previousTick = MyCarTelemetry.CarPositions.Pop();
            Piece currentPiece = this.GameManager.TrackPieces[TrackPieceIndexHelper.GetTrackIndex(currentTick.PiecePosition.Index, GameManager.TrackPieces.Length)];
            Piece previousPiece = this.GameManager.TrackPieces[TrackPieceIndexHelper.GetTrackIndex(previousTick.PiecePosition.Index, GameManager.TrackPieces.Length)];

            MyCarTelemetry.CarPositions.Push(previousTick);
            MyCarTelemetry.CarPositions.Push(currentTick);

            float coveredDistance = currentTick.PiecePosition.InPieceDistance - previousTick.PiecePosition.InPieceDistance;

            // If transitioned into a new piece adjust covered distance
            if(coveredDistance < 0)
            {
                if(previousPiece.Angle < 0.0f || previousPiece.Angle > 0.0f)
                {
                    TrackLane lane = GameManager.Track.Lanes[previousTick.PiecePosition.Lane.StartLaneIndex];
                    int radius = previousPiece.Radius;
                    int adjustedRadius = lane.DistancefromCenter + radius;

                    var cornerDistance = (float) (((2 * Math.PI) * adjustedRadius) / 8.0f);
                    coveredDistance += cornerDistance;
                }
                else
                {
                    coveredDistance = previousPiece.Length;
                }
            }
            return coveredDistance / 10.0f;
        }

        public void CollectCarPositionData(string contestantName, int gameTick, CarPosition carPosition, Piece currentPiece)
        {
            CarTelemetries[contestantName].CarPositions.Push(carPosition);
            _streamWriter.WriteLine(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};", 
                contestantName, 
                gameTick,
                carPosition.PiecePosition.Lap,
                carPosition.Angle,
                carPosition.PiecePosition.Index,
                currentPiece.Length,
                currentPiece.Angle,
                currentPiece.Radius,
                currentPiece.Switch,
                carPosition.PiecePosition.InPieceDistance));
        }

        public void CollectThrottleData(float throttle, TrottleType trottleType)
        {
            
        }

        public void Dispose()
        {
            this.Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_streamWriter != null)
                {
                    _streamWriter.Close();
                    _streamWriter.Dispose();
                    _streamWriter = null;
                }
                if (_fileStream != null)
                {
                    _fileStream.Close();
                    _fileStream.Dispose();
                    _fileStream = null;
                }
            }
        }
    }
}
