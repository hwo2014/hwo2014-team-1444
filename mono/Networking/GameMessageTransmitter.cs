﻿using System.IO;

namespace HelloWorldOpen.Bot.Networking
{
    public class GameMessageTransmitter : IMessageTransmitter
    {
        private readonly StreamWriter _streamWriter;

        public GameMessageTransmitter(StreamWriter streamWriter)
        {
            this._streamWriter = streamWriter;
            this._streamWriter.AutoFlush = true;
        }

        #region Implementation of IMessageTransmitter

        public void Transmit(string message)
        {
            this._streamWriter.WriteLine(message);
        }

        #endregion
    }
}
