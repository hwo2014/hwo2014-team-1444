﻿namespace HelloWorldOpen.Bot.Networking
{
    public interface INetworkStream
    {
        IMessageReciever GetMessageReciever();

        IMessageTransmitter GetMessageTransmitter();
    }
}
