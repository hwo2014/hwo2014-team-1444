﻿using System;
using System.Net.Sockets;

namespace HelloWorldOpen.Bot.Networking
{
    public class GameTcpClient : ITcpClient
    {
        private TcpClient _client;

        public GameTcpClient(string host, int port)
        {
            this._client = new TcpClient(host, port);
        }

        #region Implementation of IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        public INetworkStream GetStream()
        {
            return new GameNetworkStream(_client.GetStream());
        }

        #endregion

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if(_client != null)
                {
                    ((IDisposable)_client).Dispose();
                    _client = null;
                }
            }
        }
    }
}
