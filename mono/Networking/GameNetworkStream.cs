﻿using System.IO;
using System.Net.Sockets;

namespace HelloWorldOpen.Bot.Networking
{
    public class GameNetworkStream : INetworkStream
    {
        private readonly NetworkStream _networkStream;

        public GameNetworkStream(NetworkStream networkStream)
        {
            //TODO: Guard.
            this._networkStream = networkStream;
        }

        #region Implementation of INetworkStream

        public IMessageReciever GetMessageReciever()
        {
            return new GameMessageReciever(new StreamReader(_networkStream));
        }

        public IMessageTransmitter GetMessageTransmitter()
        {
            return new GameMessageTransmitter(new StreamWriter(_networkStream));
        }

        #endregion
    }
}
