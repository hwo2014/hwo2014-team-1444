﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelloWorldOpen.Bot.Networking
{
    public interface IMessageTransmitter
    {
        void Transmit(string message);
    }
}
