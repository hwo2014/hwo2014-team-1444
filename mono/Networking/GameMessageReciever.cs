﻿using System.IO;

namespace HelloWorldOpen.Bot.Networking
{
    public class GameMessageReciever : IMessageReciever
    {
        private readonly StreamReader _streamReader;

        public GameMessageReciever(StreamReader streamReader)
        {
            this._streamReader = streamReader;
        }

        #region Implementation of IMessageReciever

        public string RecieveMessage()
        {
            return this._streamReader.ReadLine();
        }

        #endregion
    }
}
