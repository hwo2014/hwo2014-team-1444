﻿using System;

namespace HelloWorldOpen.Bot.Networking
{
    public interface ITcpClient : IDisposable
    {
        INetworkStream GetStream();
    }
}
