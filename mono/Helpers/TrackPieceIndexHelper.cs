﻿namespace HelloWorldOpen.Bot.Helpers
{
    public static class TrackPieceIndexHelper
    {
        public static int GetTrackIndex(int index, int trackLength)
        {
            return index % trackLength;
        }
    }
}
