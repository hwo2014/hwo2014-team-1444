﻿using System;
using System.IO;
using HelloWorldOpen.Bot.Abstract;
using HelloWorldOpen.Bot.Managers;
using HelloWorldOpen.Bot.Messages;
using HelloWorldOpen.Bot.Models;
using HelloWorldOpen.Bot.Networking;
using Newtonsoft.Json;

namespace HelloWorldOpen.Bot
{
    public class Bot
    {
        private readonly GameManager _gameManager;

        public Bot(GameManager gameManager)
        {
            this._gameManager = gameManager;
        }

        public void Throttle(int gameTick, float currentSpeed, Piece[] nextTrackPieces)
        {
            float throttle = 0.0f;
            float optimalSpeed1 = CalculateOptimalSpeedForPiece(nextTrackPieces[0]);
            float optimalSpeed2 = CalculateOptimalSpeedForPiece(nextTrackPieces[1]);
            float optimalSpeed3 = CalculateOptimalSpeedForPiece(nextTrackPieces[2]);
            float optimalSpeed4 = CalculateOptimalSpeedForPiece(nextTrackPieces[3]);
            float optimalSpeed5 = CalculateOptimalSpeedForPiece(nextTrackPieces[4]);
            float averageOptiomalSpeed = (optimalSpeed1 + optimalSpeed2 + optimalSpeed3 + optimalSpeed4 + optimalSpeed5) / 5.0f;

            //Console.WriteLine(currentSpeed);
            //Console.WriteLine("averageOptiomalSpeed: {0}", averageOptiomalSpeed);
            //if (currentSpeed < optimalSpeed3 && currentSpeed < optimalSpeed4 && currentSpeed < optimalSpeed5)
            //{
            //    throttle = 1.0f;
            //}
            if (currentSpeed < optimalSpeed3 && currentSpeed < optimalSpeed4 && currentSpeed < optimalSpeed5)
            {
                throttle = 1.0f;
            }
            else
            {
                throttle = optimalSpeed5 * 0.65f;
            }

            //if (optimalSpeed3 < averageOptiomalSpeed)
            //{
            //    speed = 1.0f;
            //}
            //else if (currentSpeed > averageOptiomalSpeed)
            //{
            //    speed = 0.4f;
            //}
            Console.WriteLine(throttle);
            var throttleMessage = new Throttle(throttle);
            throttleMessage.GameTick = gameTick;
            _gameManager.SendMessage(throttleMessage);
        }

        public static float CalculateOptimalSpeedForPiece(Piece piece)
        {
            if (piece.Angle >= -45.0f && piece.Angle < -22.5f || piece.Angle <= 45.0f && piece.Angle > 22.5f)
            {
                return 0.65f;
            }
            if (piece.Angle >= -22.5f && piece.Angle < -1.0f || piece.Angle <= 22.5f && piece.Angle > 1.0f)
            {
                return 0.8f;
            }
            if (piece.Angle >= -1.0f && piece.Angle <= 1.0f)
            {
                return 1.0f;
            }
            return 0.65f;
        }

        private int GetPieceIndex(int index)
        {
            return index % _gameManager.Race.Track.Pieces.Length;
        }
    }
}
