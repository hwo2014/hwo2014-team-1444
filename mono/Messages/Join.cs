﻿using HelloWorldOpen.Bot.Abstract;
using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Messages
{
    public class Join : SendMessage
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("color")]
        public string Color { get; set; }

        public Join(string name, string key)
        {
            this.Name = name;
            this.Key = key;
            this.Color = "red";
            this.MessageType = "join";
        }
    }
}
