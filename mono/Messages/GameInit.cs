﻿using HelloWorldOpen.Bot.Abstract;
using HelloWorldOpen.Bot.Models;
using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Messages
{
    public class GameInit
    {
        [JsonProperty("race")]
        public Race Race { get; set; }
    }
}
