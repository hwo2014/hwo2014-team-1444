﻿using System;
using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Messages
{
    public class MessageWrapper
    {
        [JsonProperty("msgType")]
        public string MessageType { get; set; }

        [JsonProperty("data")]
        public Object Data { get; set; }

        [JsonProperty("gameId")]
        public string GameId { get; set; }

        [JsonProperty("gameTick")]
        public int GameTick { get; set; }

        public MessageWrapper(string messageType, Object data, int gameTick)
        {
            this.MessageType = messageType;
            this.Data = data;
            this.GameTick = gameTick;
        }

        public T Deserialize<T>()
        {
            return JsonConvert.DeserializeObject<T>(this.Data.ToString());
        }
    }
}
