﻿using HelloWorldOpen.Bot.Abstract;
using HelloWorldOpen.Bot.Models;
using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Messages
{
    public class CarPositions : SendMessage
    {
        [JsonProperty("data", Required = Required.AllowNull)]
        public CarPosition[] Data { get; set; }

        [JsonProperty("gameId", Required = Required.AllowNull)]
        public string GameId { get; set; }

        [JsonProperty("gameTick", Required = Required.AllowNull)]
        public int GameTick { get; set; }
    }
}
