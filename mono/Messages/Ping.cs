﻿using HelloWorldOpen.Bot.Abstract;

namespace HelloWorldOpen.Bot.Messages
{
    public class Ping : SendMessage
    {
        public Ping()
        {
            this.MessageType = "ping";
        }
    }
}
