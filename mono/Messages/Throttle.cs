﻿using HelloWorldOpen.Bot.Abstract;
using Newtonsoft.Json;

namespace HelloWorldOpen.Bot.Messages
{
    public class Throttle : SendMessage
    {
        [JsonProperty("value")]
        public double Value { get; set; }

        public Throttle(double value)
        {
            this.Value = value;
            this.MessageType = "throttle";
        }

        protected override object GetData()
        {
            return this.Value;
        }
    }
}
