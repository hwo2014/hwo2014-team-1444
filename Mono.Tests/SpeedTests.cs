﻿using System;
using HelloWorldOpen.Bot;
using HelloWorldOpen.Bot.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Mono.Tests
{
    [TestClass]
    public class SpeedTests
    {
        [TestMethod]
        public void Speed_FullSpeed_Test()
        {
            // Arrange
            var piece = new Piece()
                        {
                            Angle = 0.0f
                        };

            // Act
            float speed = Bot.CalculateOptimalSpeedForPiece(piece);

            // Assert
            Assert.AreEqual(1.0f, speed);
        }

        [TestMethod]
        public void Speed_MediumSpeed_Test()
        {
            // Arrange
            var leftTurn = new Piece()
            {
                Angle = 22.5f
            };
            var rightTurn = new Piece()
            {
                Angle = -22.5f
            };

            // Act
            float leftTurnSpeed = Bot.CalculateOptimalSpeedForPiece(leftTurn);
            float rightTurnSpeed = Bot.CalculateOptimalSpeedForPiece(rightTurn);

            // Assert
            Assert.AreEqual(0.8f, leftTurnSpeed);
            Assert.AreEqual(0.8f, rightTurnSpeed);
        }

        [TestMethod]
        public void Speed_CornerSpeed_Test()
        {
            // Arrange
            var leftTurn = new Piece()
            {
                Angle = 45.0f
            };
            var rightTurn = new Piece()
            {
                Angle = -45.0f
            };

            // Act
            float leftTurnSpeed = Bot.CalculateOptimalSpeedForPiece(leftTurn);
            float rightTurnSpeed = Bot.CalculateOptimalSpeedForPiece(rightTurn);

            // Assert
            Assert.AreEqual(0.6f, leftTurnSpeed);
            Assert.AreEqual(0.6f, rightTurnSpeed);
        }

        [TestMethod]
        public void Speed_Roads_SwitchCornerCorner_Test()
        {
            // Arrange
            var piece1 = new Piece() { };
            var piece2 = new Piece() { Switch = true, };
            var piece3 = new Piece() { Angle = 45.0f, };


            Assert.Inconclusive();
            // Act
            //float speed = Bot.CalculateSpeedForPieces(piece1, piece2, piece3, null, null);
            
            
            // Assert
            //Assert.AreEqual(0.8f, speed);
        }
    }
}
